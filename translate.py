import ruamel.yaml

def main():
    import argparse
    parser = argparse.ArgumentParser(
        prog='GitlabCI-MD',
        description='Translate gitlab-ci.yml to markdown')
    parser.add_argument('file', help='gitlab-ci.yml file')
    parser.add_argument('-o', '--output', help='output file')
    args = parser.parse_args()

    file = args.file

    try:
        with open(file, 'r') as stream:
            try:
                data = ruamel.yaml.load(stream, ruamel.yaml.RoundTripLoader)
            except yaml.YAMLError as exc:
                print(exc)
                exit(2)
    except IOError:
        print("File not accessible")
        exit(3)

    variables = data.get('variables', {})
    stages = data.get('stages', [])
    cache = data.get('cache', {})
    includes = data.get('include', {})
    macro = []
    jobs = []

    for k, v in data.items():
        if "stage" in v:
            jobs.append((k,v))
        if k.startswith('.'):
            macro.append((k,v))


    final = ""
    final += "# Gitlab CI/CD\n\n"

    if includes:
        final += "## Includes\n\n"
        if isinstance(includes, str):
            final += f"* [{includes}]({includes})\n\n"
        else:
            for include in includes:
                if include.get('local'):
                    final += f"* Local file: [{include.get('local')}]({include.get('local')})\n"
                if include.get('template'):
                    final += f"* Template: [{include.get('template')}]({include.get('template')})\n"
                if include.get('remote'):
                    final += f"* Remote file: [{include.get('remote')}]({include.get('remote')})\n"
                if include.get('project'):
                    final += f"* Project: [{include.get('project')}]({include.get('project')})"
                    if include.get('ref'):
                        final += f"@{include.get('ref')}"
                    final += "\n"
                if include.get('file'):
                    final += f"\t* *File*: [{include.get('file')}]({include.get('file')})\n"

        final += "\n"

    if variables:
        final += "## Variables\n\n"

        final +="| Name | Value |\n"
        final +="| ---- | ----- |\n"
        for key, value in variables.items():
            final += f"| {key} | `{value}` |\n"
        final += "\n"

    if stages:
        final += "## Stages\n\n"
        for stage in stages:
            final += f"* <a id={stage}>`{stage}`</a>\n"

            for job in jobs:
                (job_name, job_data) = job
                if job_data.get('stage') == stage:
                    final += f"\t* [{job_name}](#{job_name})\n"
        final += "\n"

    if cache:
        final += "## Cache\n\n"
        if cache.get("paths"):
            final += "* Paths:\n"
            for path in cache.get("paths"):
                final += f"\t* `{path}`\n"
        if cache.get('policy'):
            final += f"* Policy: `{cache.get('policy')}`\n"
        if cache.get("when"):
            inputs = {
                'on_success': 'Save the cache only when the job succeeds.',
                'on_failure': 'Save the cache only when the job fails.',
                'always': 'Always save the cache.'
            }
            final += f"* When: `{cache.get('when')}` - {inputs.get(cache.get('when'))}\n\n"

    if jobs:
        final += "## Jobs\n\n"
        for job in jobs:
            (job_name, job_data) = job
            final += f"### <a id={job_name}>{job_name}</a>\n\n"
            final +=f"* Stage: [{job_data.get('stage')}](#{job_data.get('stage')})\n"

            if job_data.get('image'):
                if isinstance(job_data.get('image'), str):
                    final += f"* Image: `{job_data.get('image')}`\n"
                else:
                    img = job_data.get('image')
                    final += f"* Image: \n\t- Name: `{img['name']}`\n"
                    if img.get('entrypoint'):
                        final += f"\t* Entrypoint: `{img['entrypoint']}`\n"
                    if img.get('args'):
                        final += f"\t* Args: `{img['args']}`\n"


            if job_data.get('only'):
                final += "* Only:\n"
                for only in job_data.get('only'):
                    final += f"\t* `{only}`\n"

            if job_data.get('before_script'):
                final += "* Before script:\n```bash\n"
                for script in job_data.get('before_script'):
                    final += f"{script}\n"
                final += "```\n\n"

            if job_data.get('script'):
                final += "* Script:\n```bash\n"
                for script in job_data.get('script'):
                    final += f"{script}\n"
                final += "```\n\n"

            if job_data.get('environment'):
                final+="* Environment:\n"
                for key, value in job_data.get('environment').items():
                    final += f"\t* {key}: `{value}`\n"
                final+="\n"

    if args.output:
        with open(args.output, "w") as f:
            f.write(final)
    else:
        print(final)


if __name__ == '__main__':
    main()
