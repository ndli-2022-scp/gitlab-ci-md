# Gitlab-CI-MD


## ℹ️ Information

Projet créé pendant la nuit de l'info 2022 pour le défis: "Expert en documentation GitLab tu deviendras"

Sujet: *Votre défi est d’écrire un script afin de convertir un fichier de CI/CD GitLab .gitlab-ci.yml en un fichier Markdown.*

## 📝 Description

Le script permet de convertir un fichier .gitlab-ci.yml en un fichier README.md

## 📦 Installation

Lancer le programme grâce à docker:

```bash
docker run  -v $(pwd):/app -w /app -t --rm cleymax/gitlab-ci-md:latest .gitlab-ci.yml -o gitlab-ci.md
```

Un fichier gitlab-ci.md sera créé dans le dossier courant.


Nous avons aussi la possibilité de lancer le programme en local:

```bash
git clone https://gitlab.com/ndli-2022-scp/gitlab-ci-md.git
cd gitlab-ci-md
pip install -r requirements.txt
python main.py .gitlab-ci.yml -o gitlab-ci.md
```

## 🟢 CI

Le projet est intégré à Gitlab-CI et est déployé sur DockerHub.

Vous pouvez aussi intégré le projet à votre Gitlab-CI en ajoutant le fichier .gitlab-ci.yml:

```yaml
generate-ci-doc:
    image:
        name: cleymax/gitlab-ci-md:latest
        entrypoint: ["python", "translate.py", ".gitlab-ci.yml", "-o", "gitlab-ci.md"]
    artifacts:
        paths:
            - gitlab-ci.md
```

## 🙍 Auteur

- Clément PERRIN ([@Cleymax](https://gitlab.com/cleymax))